# coinmiles-backend-interview

**This tests requires you fetch affiliate transactions from three sources and store in a common collection based on the schema provided, please ask for clarification either on Zoom, Slack or Email**


- Process these transactions by combining the data sources provided (from merchant networks Flex, Rakuten, and Impact), and store in a MongoDB collection.
- The `amount` is the commission allocated to the user and should be 50% of the `totalCommision`, `commision` goes to the company and accounts for the other 50%
- The `userId` field is based on the map below, for example for the flex data you will store the `subid` as `userId`
- `lockingDate` should be 90 days from the time of purchase
- Ensure that one sale === 1 transaction, if records are for SKUs they must be combined appropriately
- Write an automated test to ensure its working
- This task should run every hour

**Contraints**
- Use Typescript and MongoDB

**userId map**

```
flex: subid
rak: u1
impact: SubId1

```

Data Sources
```
https://855dgl99sf.execute-api.ca-central-1.amazonaws.com/dev/system-rest-api/test-api/v1/flex/allsales
https://855dgl99sf.execute-api.ca-central-1.amazonaws.com/dev/system-rest-api/test-api/v1/impact/Actions
https://855dgl99sf.execute-api.ca-central-1.amazonaws.com/dev/system-rest-api/test-api/v1/rak/transactions
```

MongoDB Connection

```
mongo "mongodb+srv://cluster0.rv675.mongodb.net/<dbname>" --username test_user
password:Aido1Fz2UuMQ7JMn
```


Schema
```
  advertiserName: string;
  userId: string;
  postedDate: string;
  clickDate: string;
  lockingDate: string;
  orderNumber: string;
  totalSaleAmount: number;
  totalCommission: number;
  amount: number
  commission: number;
  
```
